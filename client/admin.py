from django.contrib import admin
from .models import Client, Recipe, CommentToRecipe, CommentToIngredient, Cart
# Register your models here.
@admin.register(Client, Recipe, CommentToRecipe, CommentToIngredient, Cart)
class ClientAdmin(admin.ModelAdmin):
    pass
