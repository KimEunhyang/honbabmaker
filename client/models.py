from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from partner.models import HashTag, Ingredient, Package

# Create your models here.
class  Client(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE )
    name = models.CharField(
            max_length=50,
            verbose_name="고객 이름",
        )

    def __str__(self):
        return self.name

class Recipe(models.Model):
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        verbose_name="ID",
    )
    title = models.CharField(
        max_length = 20,
        verbose_name="레시피명",
        )
    introduction = models.CharField(
        max_length = 100,
        verbose_name="레시피 소개",
        )
    image = models.ImageField(
        verbose_name="레시피 이미지",
    )
    hashtag = models.ManyToManyField(HashTag)
    created_date = models.DateTimeField(default=timezone.now)
    hit =  models.PositiveIntegerField()
    def __str__(self): #def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("recipe_detail", kwargs={"recipe_id":self.id})

class Recipe_stage(models.Model):
    recipe = models.ForeignKey(
        Recipe,
        on_delete=models.CASCADE,
    )
    stage = models.PositiveSmallIntegerField(
        verbose_name="단계",
    )
    title = models.CharField(
        max_length = 20,
        verbose_name="단계명",
        )
    image = models.ImageField(
        verbose_name="레시피 이미지",
    )
    content = models.TextField(
        verbose_name="레시피 내용",
    )
    def __str__(self):
        return self.title

class CommentToRecipe(models.Model):
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        verbose_name="ID",
    )
    recipe = models.ForeignKey(
        Recipe,
        related_name = "recipe_comments",
        on_delete = models.CASCADE
    )
    comment = models.CharField(max_length = 200)
    score = models.PositiveSmallIntegerField(
        verbose_name="별점",
    )
    created_date = models.DateTimeField(default=timezone.now)

    # def approve(self):
    # self.approved_comment = True
    # self.save()

    def __str__(self):
        return "{} 댓글: {}".format(self.recipe.title, self.comment)


class CommentToIngredient(models.Model):
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        verbose_name="ID",
    )
    Ingredient = models.ForeignKey(
        Ingredient,
        related_name = "ingredient_comments",
        on_delete = models.CASCADE
    )
    comment = models.CharField(max_length = 200)
    score = models.PositiveSmallIntegerField(
        verbose_name="별점",
    )
    created_date = models.DateTimeField(default=timezone.now)

    # def approve(self):
    # self.approved_comment = True
    # self.save()

    def __str__(self):
        return "{} 댓글: {}".format(self.Ingredient.name, self.comment)

class IngredientItem(models.Model):
    item = models.ForeignKey(Ingredient)
    quantity = models.PositiveIntegerField(default=1)
    line_item_total = models.DecimalField(max_digits=10, decimal_places=2)

    def __unicode__(self):
    	return self.item.title

    def remove(self):
    	return self.item.remove_from_cart()

class PackageItem(models.Model):
    item = models.ForeignKey(Package)
    quantity = models.PositiveIntegerField(default=1)
    line_item_total = models.DecimalField(max_digits=10, decimal_places=2)

    def __unicode__(self):
    	return self.item.title

    def remove(self):
    	return self.item.remove_from_cart()

class Cart(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    ingredient_items = models.ManyToManyField(IngredientItem, blank=True)
    package_items = models.ManyToManyField(PackageItem, blank=True)
    total = models.DecimalField(max_digits=50, decimal_places=2, default=25.00)

    def __str__(self):
        return "고객 :{}".format(self.client.username)

class Order(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    address = models.CharField(
        max_length = 100,
        verbose_name = "주소",
    )
    created_at = models.DateTimeField(auto_now_add=True)
    ingredients = models.ManyToManyField(
        Ingredient,
        through = 'Ordertime',
        through_fields = ('order', 'Ingredient'),
    )
    packages = models.ManyToManyField(
        Package,
        through = 'Ordertime',
        through_fields = ('order', 'Package'),
    )

    def __str__(self):
        return "고객 :{} 주소:{}".format(self.client.username, self.address)

class Ordertime(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    Ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE)
    Package = models.ForeignKey(Package, on_delete=models.CASCADE)
    count = models.PositiveSmallIntegerField()

    def __str__(self):
        return "메뉴 :{} 수량:{}".format(self.menu.name, self.count)
