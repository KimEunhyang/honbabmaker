from django import forms
from django.forms import ModelForm, Textarea, TextInput, ChoiceField
from .models import Recipe, Recipe_stage

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = (
            "image",
            "title",
            "introduction",
            "hit",
        )
        widgets = {
            "title": TextInput(attrs={"class":"form-control"}),
            "introduction": Textarea(attrs={"class":"form-control"}),
            "hit": TextInput(attrs={"type":"hidden", "value":"0"}),
        }

class RecipeStageForm(ModelForm):
    class Meta:
        model = Recipe_stage
        fields = (
            "image",
            "title",
            "content",
            "stage",
        )
        widgets = {
            "title": TextInput(attrs={"class":"form-control"}),
            "content": Textarea(attrs={"class":"form-control"}),
            "stage": TextInput(attrs={"type":"hidden", "value":"0"}),
        }
