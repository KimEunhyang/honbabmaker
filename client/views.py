from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.contrib.auth import (
    authenticate,
    login as auth_login,
    logout as auth_logout,
)
from django.core import serializers
from django.http import HttpResponse

from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Client, Recipe, CommentToRecipe, CommentToIngredient, IngredientItem, PackageItem, Cart
from partner.models import Partner, Ingredient, Package, HashTag
from .forms import RecipeForm, RecipeStageForm
from django.utils import timezone

class RecipeListView(ListView):
    model = Recipe

    def get_context_data(self, *args, **kwargs):
        context = super(RecipeListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class ClientPackageListView(ListView):
    model = Package
    template_name = 'client/client_package_list.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ClientPackageListView, self).get_context_data(*args, **kwargs)
        context['now'] = timezone.now()
        return context

    def get_queryset(self, *args, **kwargs):
        qs = super(ClientPackageListView, self).get_queryset(*args, **kwargs)
        category = self.request.GET.get("category")
        if not category:
            qs = Package.objects.all()
        else:
            qs = Package.objects.filter(category = category)
        return qs

class PackageDetailView(DetailView):
    model = Package
    pk_field= 'package_id'
    template_name = "client/package_detail.html"
    #template_name = "<appname>/<modelname>_detail.html"
    def get_context_data(self, *args, **kwargs):
    	context = super(PackageDetailView, self).get_context_data(*args, **kwargs)
    	instance = self.get_object()
    	return context


def package_detail_view_func(request, package_id):
	#product_instance = Product.objects.get(id=id)
	package = get_object_or_404(Package, id=package_id)
	try:
		package = Package.objects.get(id=package_id)
	except Package.DoesNotExist:
		raise Http404
	except:
		raise Http404

	template = "client/package_detail.html"
	ctx = {
		"package": package
	}
	return render(request, template, ctx)


URL_LOGIN = "/login/"

def client_group_check(user):
    return "client" in [group.name for group in user.groups.all()]

# Create your views here.
def index(request):
    ctx = {}
    ingredient_list = Ingredient.objects.all()
    package_list = Package.objects.all()
    hashtag_list = HashTag.objects.all()
    for package in package_list:
        print(package.name)
    ctx.update({
    "ingredient_list":ingredient_list,
    "package_list":package_list,
    "hashtag_list":hashtag_list,
    })
    return render(request, 'main.html', ctx)

def common_login(request, ctx):
    if request.method == "GET":
        pass
    elif request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(username=username, password=password)

        if user is not None:
            # if group not in [group.name for group in user.groups.all()]:
            #     ctx.update({"error" : "접근 권한이 없습니다."})
            #     for group in user.groups.all():
            #         print("group:",group)
            # else:
            group_list = [group.name for group in user.groups.all()]
            ctx.update({ "group_list" : group_list })
            auth_login(request, user)
            next_value = request.GET.get("next")
            if next_value:
                return redirect(next_value)
            else:
                if "partner" in group_list:
                    return redirect("/partner/")
                else:
                    return redirect("/")

        else:
            ctx.update({"error" : "사용자가 없습니다."})

    return render(request, "login.html", ctx)

def login(request):
    ctx = {"is_client":True}
    return common_login(request, ctx)

# username같을때 에러처리필요
def common_signup(request, ctx):
    if request.method == "GET":
        pass
    elif request.method == "POST":
        print('-------------------POST-------------------')
        name = request.POST.get("username")
        email = request.POST.get("email")
        password = request.POST.get("password")
        group = request.POST.get("group")
        # print(username, email, password)

        user = User.objects.create_user(name, email, password)
        print('-------------------유저생성-------------------')
        target_group = Group.objects.get(name=group)
        user.groups.add(target_group)
        if group == "client":
            Client.objects.create(user=user, name=name)

        # Article.objects.create(title="", content="")
    return render(request, "signup.html", ctx)

# def signup(request):
#     ctx = {"is_client":True}
#     return common_signup(request, ctx, "client")

def signup(request):
    ctx = {}
    return common_signup(request, ctx)

def success(request):
    ctx = {}
    return render(request, "signup_success.html", ctx)

def logout(request):
    auth_logout(request)
    return redirect("/")

def ingredient(request):
    ctx = {}
    if request.method == "GET":
        category = request.GET.get("category")
        if not category:
            ingredient_list = Ingredient.objects.all()
        else:
            ingredient_list = Ingredient.objects.filter(category = category)

        category_list = set([
        (ingredient.category, ingredient.get_category_display())
        for ingredient in ingredient_list
        ])

        ctx.update({
        "ingredient_list":ingredient_list,
        "category_list":category_list,
        })

    elif request.method == "POST":
        pass
    return render(request, 'client_ingredient_list.html', ctx)



def ajax_ingredient(request):
    ctx = {}
    ingredient_list = Ingredient.objects.all()
    if request.method == "POST":
        ingredient_id = request.POST.get('ingredient_id')
        ingredient = Ingredient.objects.filter(id = ingredient_id)
        comments = Ingredient.ingredient_comments
        ingredient_json = serializers.serialize("json", ingredient)
    return HttpResponse(ingredient_json, content_type='application/json')


def recipe(request):
    ctx = {}
    recipe_list = Recipe.objects.all()

    if request.method == "GET":
        ctx.update({
        "recipe_list":recipe_list,
        })
    elif request.method == "POST":
        pass
    return render(request, 'recipe_list.html', ctx)

def recipe_add(request):
    ctx = {}
    if request.method == "GET":
        recipe_form = RecipeForm()
        ctx.update({
            "recipe_form":recipe_form,
        })
    elif request.method == "POST":
        print('----------------Post성공----------------')
        hashtag_set = ([])
        hashtag = request.POST.get('hashtag')
        hashtag_set = hashtag.split()
        recipe_form = RecipeForm(request.POST,request.FILES)
        if recipe_form.is_valid():
            print('----------------레시피 체크----------------')
            recipe = recipe_form.save(commit = False)
            recipe.client = request.user.client
            recipe.save()
            for hashtag in hashtag_set:
                if hashtag not in [hashtag.name for hashtag in HashTag.objects.all()]:
                    HashTag.objects.create(
                        name = hashtag
                    )
            recipe.hashtag.add(hashtag)
            return redirect("/myrecipe/")
        else:
            ctx.update({"recipe_form":recipe_form})

    return render(request, "recipe_add.html", ctx)

def myrecipe(request):
    ctx = {}
    recipe_list  =  Recipe.objects.filter(client = request.user.client)
    ctx.update({"recipe_list":recipe_list})
    return render(request, "my_recipe_list.html",ctx)

def recipe_detail(request,recipe_id):
    ctx = {}
    recipe = Recipe.objects.get(id = recipe_id)
    ctx.update({"recipe":recipe})
    if request.method == 'POST' :
        client = request.user.client
        score = request.POST.get('score')
        comment = request.POST.get('comment')
        CommentToRecipe.objects.create(
            client = client,
            recipe = recipe,
            comment = comment,
            score =  score
        )
        return HttpResponseRedirect('/recipe/{}/'.format(recipe_id))
    return render(request, "recipe_detail.html", ctx)



@user_passes_test(client_group_check, login_url = URL_LOGIN)
@login_required(login_url = URL_LOGIN)
def recipe_edit(request, recipe_id):
    ctx = {}
    recipe = Recipe.objects.get(id = recipe_id)
    if request.method == "GET":
        # Partnerform 객체 생성
        recipe_form = RecipeForm(instance = recipe)
        # 태워보냄
        ctx.update({"recipe_form":recipe_form})
    elif request.method == "POST":
        recipe_form = RecipeForm(request.POST,instance=recipe)
        if recipe_form.is_valid():
            recipe = recipe_form.save(commit = False)
            recipe.client = request.user.client
            recipe.save()
            return redirect("/myrecipe/" + recipe_id)
        else:
            ctx.update({"recipe_form":recipe_form})

    return render(request, "recipe_add.html", ctx)

@user_passes_test(client_group_check, login_url = URL_LOGIN)
@login_required(login_url = URL_LOGIN)
def recipe_delete(request, recipe_id):
    recipe = Recipe.objects.get(id = recipe_id)
    recipe.delete()
    return redirect("/myrecipe/")

def order(request, item, item_id):
    ctx = {}
    if item == 'ingredient':
        item = Ingredient.objects.get(id = item_id)
        print(item.name)
    else:
        item = Package.objects.get(id = item_id)
    # print(ingredient.price)
    if request.method == "GET":
        ctx.update({
        "item":item,
        })
    elif request.method == "POST":
        order = Order.objects.create(
            client = request.user.client,
            address = 'test'
            )
        for menu in menu_list:
            menu_count = request.POST.get(str(menu.id))
            menu_count = int(menu_count)
            if menu_count > 0 :
                items = Ordertime.objects.create(
                    order = order,
                    menu = menu,
                    count = menu_count
                )
        return redirect("/")

    return render(request, "order_list.html", ctx)

def cart(request, item, item_id):
    print(item_id)
    ctx = {}
    if item == 'ingredient':
        ingredient_item = Ingredient.objects.get(id = item_id)
        item = IngredientItem.objects.create(
            item = ingredient_item
        )
    else:
        package_item = Package.objects.get(id = item_id)
        item = PackageItem.objects.create(
            item = package_item
        )

    # print(ingredient.price)
    item_json = serializers.serialize("json", cart)
    return HttpResponse(item_json, content_type='application/json')

@user_passes_test(client_group_check, login_url = URL_LOGIN)
@login_required(login_url = URL_LOGIN)
def ingredient_order(request, ingredient_id):
    return order(request,'ingredient',ingredient_id)

@user_passes_test(client_group_check, login_url = URL_LOGIN)
@login_required(login_url = URL_LOGIN)
def package_order(request, package_id):
    return order(request,'package',package_id)

@user_passes_test(client_group_check, login_url = URL_LOGIN)
@login_required(login_url = URL_LOGIN)
def package_cart(request):
    if request.method == "POST":
        package_id = request.POST.get('item_id')
    return cart(request, 'package', package_id)

@user_passes_test(client_group_check, login_url = URL_LOGIN)
@login_required(login_url = URL_LOGIN)
def ingredient_cart(request):
    if request.method == "POST":
        ingredient_id = request.POST.get('item_id')
    return cart(request, 'ingredient', ingredient_id)
