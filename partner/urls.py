from django.conf.urls import url
from .views import (
    index,
    edit_info,
    PackageListView, package_add, package_edit, package_delete,
    ingredient, ingredient_delete, ingredient_edit
 )
urlpatterns = [
     url(r'^$', index, name='index'),
     url(r'^edit/$', edit_info, name='edit_info'),
     url(r'^ingredient/$', ingredient, name='ingredient'),
     url(r'^ingredient/(?P<ingredient_id>\d+)/delete/$', ingredient_delete, name='ingredient_delete'),
     url(r'^ingredient/(?P<ingredient_id>\d+)/edit/$', ingredient_edit, name='ingredient_edit'),
     url(r'^package/$', PackageListView.as_view(), name='packages'),
     url(r'^package/add$', package_add, name='package_add'),
     url(r'^package/(?P<package_id>\d+)/delete/$', package_delete, name='package_delete'),
     url(r'^package/(?P<package_id>\d+)/edit/$', package_edit, name='package_edit'),

    #  url(r'^ingredient/add$', ingredient_add, name='ingredient_add'),
]
