from django.contrib import admin
from .models import Partner, HashTag, Ingredient, Package

# Register your models here.
@admin.register(Partner, HashTag, Ingredient, Package)
class PartnerAdmin(admin.ModelAdmin):
    pass
