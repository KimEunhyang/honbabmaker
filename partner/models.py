from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Partner(models.Model):
    KOREAN = 'kr'
    CHINESE = 'ch'
    JAPANESE = 'jp'
    WESTERN ='ws'
    CATEGORY_CHOICES = (
        (KOREAN, '한식'),
        (CHINESE, '중식'),
        (JAPANESE, '일식'),
        (WESTERN, '양식'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE )
    name = models.CharField(
        max_length=50,
        verbose_name="업체 이름",
    )
    contact = models.CharField(
        max_length=50,
        verbose_name="연락처",
    )
    address = models.CharField(
        max_length=200,
        verbose_name="주소",
    )
    description = models.TextField(
        verbose_name="상세 소개",
    )
    category = models.CharField(
    max_length = 2,
    choices = CATEGORY_CHOICES,
    default = KOREAN,
    )
    def __str__(self):
        return self.name

class HashTag(models.Model):
    name = models.CharField(
        max_length = 50,
        primary_key=True
        )
    def __str__(self):
        return self.name

class Ingredient(models.Model):
    MEAT = 'mt'
    SEA_FOOD = 'sf'
    RICE = 'rc'
    KIMCHI = 'kc'
    VEGETABLE = 'vg'
    Egg = 'eg'
    PROCESS_FOOD = 'pf'
    Fruit = 'fr'
    SNACK = 'sn'
    CATEGORY_CHOICES = (
        (MEAT, '정육'),
        (SEA_FOOD, '해산물'),
        (RICE, '쌀'),
        (VEGETABLE, '채소'),
        (Egg, '알류'),
        (PROCESS_FOOD, '가공&냉장'),
        (Fruit, '과일'),
        (SNACK, '간식'),
    )
    partner =  models.ForeignKey(
        Partner,
        on_delete = models.CASCADE,
    )
    image = models.ImageField(
        verbose_name="메뉴 이미지",
    )
    name = models.CharField(
        max_length = 50,
        verbose_name="메뉴명",
        )
    price = models.PositiveIntegerField(
        verbose_name="가격",
        )
    hashtag = models.ManyToManyField(HashTag)

    category = models.CharField(
    max_length = 2,
    choices = CATEGORY_CHOICES,
    default = MEAT,
        )

    score = models.PositiveSmallIntegerField(
        verbose_name="별점",
    )
    score_string = models.CharField(
        max_length = 5,
    )
    def __str__(self):
        return self.name

class Package(models.Model):
    SIDE_DISH = 'sd'
    STEW = 'st'
    WESTERN = 'ws'
    KOREAN = 'ko'
    SIMPLE = 'sm'
    SPECIAL = 'sp'

    CATEGORY_CHOICES = (
        (SIDE_DISH, '반찬'),
        (STEW, '국/찌개'),
        (WESTERN, '양식'),
        (KOREAN, '한식'),
        (SIMPLE, '간편요리'),
        (SPECIAL, '스페셜요리'),
    )
    partner =  models.ForeignKey(
        Partner,
        on_delete = models.CASCADE,
    )

    image = models.ImageField(
        verbose_name="패키지 이미지",
    )
    name = models.CharField(
        max_length = 50,
        verbose_name="패키지명",
        )
    introduction = models.CharField(
        max_length = 100,
        verbose_name="패키지 소개",
        )
    price = models.PositiveIntegerField(
        verbose_name="가격",
        )
    hashtag = models.ManyToManyField(HashTag)

    category = models.CharField(
    max_length = 2,
    choices = CATEGORY_CHOICES,
    default = SIDE_DISH,
        )

    score = models.PositiveSmallIntegerField(
        verbose_name="별점",
    )
    score_string = models.CharField(
        max_length = 5,
    )
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("package_detail", kwargs={"pk":self.id})
