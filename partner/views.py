from django.shortcuts import render, redirect
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.utils import timezone
from client.views import common_login, common_signup

from .models import Ingredient, Package, HashTag
from .forms import PartnerForm, IngredientForm, PackageForm
# Create your views here.

class PackageListView(ListView):
    model = Package

    def get_context_data(self, *args, **kwargs):
        context = super(PackageListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class PackageDetailView(DetailView):
	model = Package
	#template_name = "product.html"
	#template_name = "<appname>/<modelname>_detail.html"
	def get_context_data(self, *args, **kwargs):
		context = super(PackageDetailView, self).get_context_data(*args, **kwargs)
		instance = self.get_object()
		#order_by("-title")
		return context


def index(request):
    ctx = {}
    if request.method == "GET":
        # Partnerform 객체 생성
        partner_form = PartnerForm()
        # 태워보냄
        ctx.update({"partner_form":partner_form})
    elif request.method == "POST":
        partner_form = PartnerForm(request.POST)
        if partner_form.is_valid():
            partner = partner_form.save(commit = False)
            partner.user = request.user
            partner.save()
            return redirect("/partner/")
        else:
            ctx.update({"partner_form":partner_form})

    return render(request, "index.html", ctx)

def edit_info(request):
    ctx = {}
    if request.method == "GET":
        # Partnerform 객체 생성
        partner_form = PartnerForm(instance=request.user.partner)
        # 태워보냄
        ctx.update({"partner_form":partner_form})
    elif request.method == "POST":
        partner_form = PartnerForm(
            request.POST,
            instance=request.user.partner
            )
        if partner_form.is_valid():
            partner = partner_form.save(commit = False)
            partner.user = request.user
            partner.save()
            return redirect("/partner/")
        else:
            ctx.update({"partner_form":partner_form})

    return render(request,"edit_info.html", ctx)

def ingredient(request):
    ctx = {}
    if request.method == "GET":
            ingredient_list = Ingredient.objects.filter(partner=request.user.partner)
            ctx.update({
                "ingredient_list":ingredient_list,
            })
            # Partnerform 객체 생성
            ingredient_form = IngredientForm()
            # 태워보냄
            ctx.update({"ingredient_form":ingredient_form})
    elif request.method == "POST":
        print('----------------Post성공----------------')
        hashtag_set = ([])
        hashtag = request.POST.get('hashtag')
        hashtag_set = hashtag.split()
        ingredient_form = IngredientForm(request.POST,request.FILES)
        if ingredient_form.is_valid():
            print('----------------식재료 체크----------------')
            ingredient = ingredient_form.save(commit = False)
            ingredient.score_string = ""
            for i in range(1,ingredient.score):
                ingredient.score_string = ingredient.score_string + str(i)
                print(ingredient.score_string)
            ingredient.partner = request.user.partner
            ingredient.save()
            for hashtag in hashtag_set:
                if hashtag not in [hashtag.name for hashtag in HashTag.objects.all()]:
                    HashTag.objects.create(
                        name = hashtag
                    )
                ingredient.hashtag.add(hashtag)
            print('----------------메뉴 등록 성공----------------')
            return redirect("/partner/ingredient/")
        else:
            print('----------------메뉴 등록 실패----------------')
            ctx.update({"ingredient_form":ingredient_form})
            return redirect("/partner/ingredient")

    return render(request, "ingredient_list.html", ctx)

def ingredient_edit(request, ingredient_id):
    ctx = {}
    ingredient = Ingredient.objects.get(id = ingredient_id)
    if request.method == "GET":
        # Partnerform 객체 생성
        ingredient_form = IngredientForm(instance = ingredient)
        # 태워보냄
        ctx.update({"ingredient_form":ingredient_form})
    elif request.method == "POST":
        ingredient_form = IngredientForm(request.POST,request.FILES,instance=ingredient)
        if ingredient_form.is_valid():
            ingredient = ingredient_form.save(commit = False)
            ingredient.score_string = ""
            for i in range(0,ingredient.score):
                ingredient.score_string = ingredient.score_string + str(i)
                print(ingredient.score_string)
            ingredient.partner = request.user.partner
            ingredient.save()
            return redirect("/partner/ingredient/")
        else:
            ctx.update({"ingredient_form":ingredient_form})

    return render(request, "ingredient_edit.html", ctx)

def ingredient_delete(request, ingredient_id):
    ingredient = Ingredient.objects.get(id = ingredient_id)
    ingredient.delete()
    return redirect("/partner/ingredient/")

def package_add(request):
    ctx = {}
    if request.method == "GET":
        package_form = PackageForm()
        ctx.update({
            "package_form":package_form,
        })
    elif request.method == "POST":
        print('----------------Post성공----------------')
        hashtag_set = ([])
        hashtag = request.POST.get('hashtag')
        hashtag_set = hashtag.split()
        package_form = PackageForm(request.POST,request.FILES)
        if package_form.is_valid():
            print('----------------패키지 체크----------------')
            package = package_form.save(commit = False)
            package.partner = request.user.partner
            package.save()
            for hashtag in hashtag_set:
                if hashtag not in [hashtag.name for hashtag in HashTag.objects.all()]:
                    HashTag.objects.create(
                        name = hashtag
                    )
            package.hashtag.add(hashtag)
            return redirect("partner/package/")
        else:
            ctx.update({"package_form": package_form})

    return render(request, "package_add.html", ctx)


def package_edit(request, package_id):
    ctx = {}
    package = Package.objects.get(id = package_id)
    if request.method == "GET":
        # Partnerform 객체 생성
        package_form = PackageForm(instance = package)
        # 태워보냄
        ctx.update({"package_form":package_form})
    elif request.method == "POST":
        package_form = PackageForm(request.POST,request.FILES,instance=package)
        if package_form.is_valid():
            package = package_form.save(commit = False)
            package.score_string = ""
            for i in range(0,package.score):
                package.score_string = package.score_string + str(i)
                print(package.score_string)
            package.partner = request.user.partner
            package.save()
            return redirect("/partner/package/")
        else:
            ctx.update({"package_form":package_form})

    return render(request, "package_add.html", ctx)

def package_delete(request, package_id):
    package = Package.objects.get(id = package_id)
    package.delete()
    return redirect("/partner/package/")
