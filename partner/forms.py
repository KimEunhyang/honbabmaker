from django import forms
from django.forms import ModelForm, Textarea, TextInput, ChoiceField
from .models import Partner, Ingredient, Package

class PartnerForm(ModelForm):
    category = forms.ChoiceField(choices=Partner.CATEGORY_CHOICES)
    class Meta:
        model = Partner
        fields = (
            "name",
            "contact",
            "address",
            "description",
            "category",
        )
        widgets = {
            "name": TextInput(attrs={"class":"form-control"}),
            "contact": TextInput(attrs={"class":"form-control"}),
            "address": TextInput(attrs={"class":"form-control"}),
            "description": Textarea(attrs={"class":"form-control"}),
            # "category": ChoiceField(attrs={"class":"form-control"}),
        }

class IngredientForm(ModelForm):
    class Meta:
        model = Ingredient
        fields = (
            "image",
            "name",
            "price",
            "category",
            "score",
            # "hashtag",
        )
        widgets = {
            # "image": TextInput(attrs={"class":"form-control"}),
            "name": TextInput(attrs={"class":"form-control"}),
            "price": TextInput(attrs={"class":"form-control"}),
            "score": TextInput(attrs={"type":"hidden", "value":"0"}),
            # "hashtag": TextInput(attrs={"class":"form-control"}),
        }

class PackageForm(ModelForm):
    class Meta:
        model = Package
        fields = (
            "image",
            "name",
            "introduction",
            "price",
            "category",
            "score",
        )
        widgets = {
            "title": TextInput(attrs={"class":"form-control"}),
            "introduction": Textarea(attrs={"class":"form-control"}),
            "price": TextInput(attrs={"class":"form-control"}),
            "score": TextInput(attrs={"type":"hidden", "value":"0"})
        }
